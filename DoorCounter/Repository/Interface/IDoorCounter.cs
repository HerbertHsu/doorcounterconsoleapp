﻿namespace DoorCounter.Repository.Interface
{
    using System.Collections.Generic;
    using Models;

    public interface IDoorCounter
    {
        void Insert(DoorCounterModel model);

        void InsertBulk(List<DoorCounterModel> list);
    }
}
