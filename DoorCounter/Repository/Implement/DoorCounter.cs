﻿namespace DoorCounter.Repository.Implement
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using Dapper;
    using Interface;
    using Models;
    using NLog;

    public class DoorCounter : IDoorCounter
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        private readonly string sqlconnectionstring = ConfigurationManager.ConnectionStrings["SqlConnection"].ConnectionString;

        private readonly string query = @"Insert Into DoorCounterReport([SiteId], [CustomerCID], [CustomerName], [Date], [Hour], [Count], [DoorDateKey], [VendorName])
                            Values(@SiteId, @CustomerCID, @CustomerName, @Date, @Hour, @Count, @DoorDateKey, @VendorName)";

        public void Insert(DoorCounterModel model)
        {
            using (SqlConnection con = new SqlConnection(sqlconnectionstring))
            {
                try
                {
                    con.Open();
                    var returnId = con.Execute(query, model);
                }
                catch (Exception ex)
                {
                    _logger.Error("DoorCounter | Insert | error message: {0}", ex.Message);
                }
            }
        }

        public void InsertBulk(List<DoorCounterModel> list)
        {
            using (SqlConnection con = new SqlConnection(sqlconnectionstring))
            {
                con.Open();
                using (var trans = con.BeginTransaction())
                {
                    try
                    {
                        var returnId = con.Execute(query, list, trans);
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        _logger.Error("DoorCounter | InsertBulk | error message: {0}", ex.Message);
                    }
                }
            }
        }
    }
}
