﻿namespace DoorCounter
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using Models;
    using Models.Custom;
    using Services;
    using System;
    using Utils;
    using NLog;

    public class Program
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private readonly string type = ConfigurationManager.AppSettings["type"];

        public AccountService accountService = new AccountService();
        public ZoneService zoneService = new ZoneService();
        public SiteService siteService = new SiteService();
        public DoorCounterService doorCounterService = new DoorCounterService();

        public static void Main(string[] args)
        {
            Logger.Info("Program starts...");

            Program p = new Program();
            p.GetDoorCounter();

            AppDomain.CurrentDomain.UnhandledException += GlobalExceptionHandler;

            Logger.Info("Program finishes.");

            //string jsonModel = JsonConvert.SerializeObject(model, Formatting.Indented);
            //Console.WriteLine(jsonModel);
            //Console.ReadLine();
        }

        private static void GlobalExceptionHandler(object sender, EventArgs args)
        {
            Logger.Fatal(args.ToString);
            Environment.Exit(1);
        }

        public void GetDoorCounter()
        {
            var list = new List<ZoneSiteModel>();

            var accounts = accountService.GetAccounts();

            var accountModels = accounts as IList<AccountModel> ?? accounts.ToList();

            if (accountModels.IsAny())
            {
                accountModels.ToList().ForEach((account) =>
                {
                    IEnumerable<dynamic> result;

                    if (type.Equals("zone"))
                    {
                        result = zoneService.GetAllZones(account.Zones_Url);
                        if (result.IsAny())
                        {
                            list.AddRange(from ZoneModel item in result.ToList()
                                          from siteId in item.Defined_In_Site_Ids
                                          let site = siteService.GetSiteModel(account.Id, siteId)
                                          select new ZoneSiteModel
                                          {
                                              AccountId = account.Id,
                                              ZoneId = item.Id,
                                              SiteId = siteId,
                                              ExternalId = site.External_Id,
                                              SiteName = site.Name
                                          });
                        }
                    }
                    else
                    {
                        result = siteService.GetAllSites(account.Sites_Url);
                        if (result.IsAny())
                        {
                            list.AddRange(from SiteModel item in result.ToList()
                                          from zoneId in item.Defined_Zone_Ids
                                          select new ZoneSiteModel
                                          {
                                              AccountId = account.Id,
                                              ZoneId = zoneId,
                                              SiteId = item.Id,
                                              ExternalId = item.External_Id,
                                              SiteName = item.Name
                                          });
                        }
                    }
                });
            }

            doorCounterService.GetDoorCounterList(list);
        }
    }
}
