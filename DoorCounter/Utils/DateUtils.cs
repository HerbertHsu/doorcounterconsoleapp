﻿namespace DoorCounter.Utils
{
    using System;

    public static class DateUtils
    {
        public static string DateToYyyyMmDd(DateTime date)
        {
            return date.ToString("yyyyMMdd");
        }
    }
}
