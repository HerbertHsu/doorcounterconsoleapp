﻿namespace DoorCounter.Utils
{
    using System.Collections.Generic;
    using System.Linq;

    public static class EnumerableUtils
    {
        public static bool IsAny<T>(this IEnumerable<T> data)
        {
            return data != null && data.Any();
        }
    }
}
