﻿namespace DoorCounter.Models
{
    public class AccountModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string Sites_Url { get; set; }

        public string Zones_Url { get; set; }
    }
}
