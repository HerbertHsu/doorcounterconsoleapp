﻿namespace DoorCounter.Models
{
    public class BusinessHoursModel
    {
        public DayModel Monday { get; set; }

        public DayModel Tuesday { get; set; }

        public DayModel Wednesday { get; set; }

        public DayModel Thursday { get; set; }

        public DayModel Friday { get; set; }

        public DayModel Saturday { get; set; }

        public DayModel Sunday { get; set; }
    }
}
