﻿namespace DoorCounter.Models.Custom
{
    public class ZoneSiteModel
    {
        public int AccountId { get; set; }

        public int ZoneId { get; set; }

        public int SiteId { get; set; }

        public string ExternalId { get; set; }

        public string SiteName { get; set; }
    }
}
