﻿namespace DoorCounter.Models
{
    using System.Collections.Generic;

    public class ZoneModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string Defined_In_Sites_Url { get; set; }

        public IEnumerable<int> Defined_In_Site_Ids { get; set; }
    }
}
