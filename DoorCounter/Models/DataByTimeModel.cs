﻿namespace DoorCounter.Models
{
    public class DataByTimeModel
    {
        public string Start { get; set; }

        public string Stop { get; set; }

        public string Count { get; set; }

        public string Avg_Dwell { get; set; }

        public string Avg_Occupancy { get; set; }
    }
}
