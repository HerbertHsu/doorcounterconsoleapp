﻿namespace DoorCounter.Models
{
    public class PrismModel
    {
        public string Accounts_Url { get; set; }

        public string Data_Url { get; set; }

        public string Version { get; set; }
    }
}
