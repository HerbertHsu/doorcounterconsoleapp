﻿namespace DoorCounter.Models
{
    /// <summary>
    /// {
    ///    "error_status_code": 403,
    ///    "error_messages": [
    ///         "you do not have permission to access the requested resource"
    ///    ]
    /// }
    /// </summary>
    public class ErrorModel
    {
        public string Error_Status_Code { get; set; }

        public string[] Error_Messages { get; set; }
    }
}
