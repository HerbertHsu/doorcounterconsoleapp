﻿namespace DoorCounter.Models
{
    public class DayModel
    {
        public string Open { get; set; }

        public string Close { get; set; }
    }
}
