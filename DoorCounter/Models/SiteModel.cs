﻿namespace DoorCounter.Models
{
    using System.Collections.Generic;

    public class SiteModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string Timezone { get; set; }

        public string External_Id { get; set; }

        public string Defined_Zones_Url { get; set; }

        public IEnumerable<int> Defined_Zone_Ids { get; set; }

        public BusinessHoursModel Business_Hours { get; set; }

        public AddressModel Address { get; set; }

        public string Reference_Image_Url { get; set; }

        public string Entry_Zone_Id { get; set; }

        public string Entry_Zone_Url { get; set; }
    }
}
