﻿namespace DoorCounter.Models
{
    public class AddressModel
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string City { get; set; }

        public string Postal_Code { get; set; }

        public string Province { get; set; }

        public string Country_Code { get; set; }
    }
}
