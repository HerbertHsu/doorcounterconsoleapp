﻿namespace DoorCounter.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Models;

    public class SiteService : BaseService
    {
        public SiteModel GetSiteModel(int accountId, int siteId)
        {
            SiteModel model = new SiteModel();

            try
            {
                using (var client = new HttpClient())
                {
                    string baseUri = string.Format("{0}{1}accounts/{2}/sites/{3}/", this.Uri, this.Version, accountId.ToString(), siteId.ToString());
                    client.BaseAddress = new Uri(baseUri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", this.Token);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.HeaderValue));
                    HttpResponseMessage response = client.GetAsync(string.Empty).Result;
                    model = response.Content.ReadAsAsync<SiteModel>().Result;
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.Error("SiteService | GetSiteModel | error message: {0}", ex.Message);
            }

            return model;
        }

        public IEnumerable<SiteModel> GetAllSites(string uri)
        {
            IEnumerable<SiteModel> sites = Enumerable.Empty<SiteModel>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(uri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", this.Token);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.HeaderValue));
                    HttpResponseMessage response = client.GetAsync(string.Empty).Result;
                    sites = response.Content.ReadAsAsync<IEnumerable<SiteModel>>().Result;
                }
            }
            catch (AggregateException ex)
            {
                Logger.Error("SiteService | GetSiteModel | AggregateException message: " + ex.InnerException);
            }

            return sites;
        }
    }
}
