﻿namespace DoorCounter.Services
{
    using System;
    using System.Configuration;
    using Dapper;
    using System.Data.SqlClient;
    using NLog;

    public abstract class BaseService
    {
        private readonly string uri = ConfigurationManager.AppSettings["prismBaseURI"];
        private readonly string version = ConfigurationManager.AppSettings["prismAPIVersion"] + "/";
        private readonly string token = ConfigurationManager.AppSettings["prismAPIToken"];
        private readonly string startDate = ConfigurationManager.AppSettings["startDate"];
        private readonly string endDate = ConfigurationManager.AppSettings["endDate"];
        private readonly bool dateRangeModeOn = Convert.ToBoolean(ConfigurationManager.AppSettings["dateRangeModeOn"]);
        private readonly string businessHourOnly = ConfigurationManager.AppSettings["businessHourOnly"];
        private readonly string period = ConfigurationManager.AppSettings["period"];
        private readonly string dataFilterBy = ConfigurationManager.AppSettings["dataFilterBy"];
        private readonly string metric = ConfigurationManager.AppSettings["metric"];
        private readonly string country = ConfigurationManager.AppSettings["country"];
        private readonly string accounts = ConfigurationManager.AppSettings["accounts"];

        private readonly string sqlconnectionstring = ConfigurationManager.ConnectionStrings["SqlConnection"].ConnectionString;

        public string HeaderValue { get { return "application/json"; } }

        public string Uri { get { return uri; } }

        public string Version { get { return version; } }

        public string Token { get { return token; } }

        public string StartDate { get { return startDate; } }

        public string EndDate { get { return endDate; } }

        public bool DateRangeModeOn { get { return dateRangeModeOn; } }

        public string BusinessHoursOnly { get { return string.Format("business_hours_only={0}", businessHourOnly); } }

        public string DataByTimeFilter { get { return string.Format("data/{0}/?", dataFilterBy); } }

        public string PeriodByHour { get { return string.Format("period={0}", period); } }

        public string MetricByAll
        {
            get
            {
                if (metric.Equals("all"))
                {
                    return "metric=count&metric=avg_dwell&metric=avg_occupancy";
                }

                return string.Format("metric={0}", metric);
            }
        }

        public string Country { get { return country; } }

        public string Accounts { get { return accounts; } }

        public ILogger Logger;

        protected BaseService()
        {
            Logger = LogManager.GetCurrentClassLogger();
        }

        public string GetTimeFilterUri(string zoneId, string siteId)
        {
            string zoneIdParamUri = string.Format("zone_id={0}", zoneId);
            string siteIdParamUri = string.Format("site_id={0}", siteId);

            DateTime dateYesterday = DateTime.Now.AddDays(-1);
            DateTime dateNow = DateTime.Now;

            /// <summary>
            /// taiwan time settings
            /// </summary>
            if (Country.Equals("tw"))
            {
                dateYesterday = DateTime.Now.AddDays(-2);
                dateNow = DateTime.Now.AddDays(-1);
            }

            DateTime startDate = new DateTime(dateYesterday.Year, dateYesterday.Month, dateYesterday.Day, 0, 0, 0);
            DateTime endDate = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 0, 0, 0);

            DateTime? latestDate = GetLatestDate();

            /// <summary>
            /// if dateRangeModeOn in App.config is true
            /// </summary>
            if (DateRangeModeOn)
            {
                if (!string.IsNullOrEmpty(StartDate))
                {
                    DateTime startDateConfig = Convert.ToDateTime(this.StartDate);
                    startDate = new DateTime(startDateConfig.Year, startDateConfig.Month, startDateConfig.Day, 0, 0, 0);
                }

                if (!string.IsNullOrEmpty(EndDate))
                {
                    DateTime endDateConfig = Convert.ToDateTime(this.EndDate);
                    endDate = new DateTime(endDateConfig.Year, endDateConfig.Month, endDateConfig.Day, 0, 0, 0);
                }
            }
            else
            {
                /// <summary>
                /// if there's a missing data from database latest date until now
                /// e.g.    latest date is 20
                ///         date now is 24
                ///         doorcounter should run its process in 23
                ///         missing data are 21 and 22
                /// </summary>
                if (latestDate.HasValue)
                {
                    int daysDiff = (dateNow.Date - latestDate.Value.Date).Days;
                    if (daysDiff > 2)
                    {
                        DateTime missingStartDate = latestDate.Value.AddDays(1);
                        startDate = new DateTime(missingStartDate.Year, missingStartDate.Month, missingStartDate.Day, 0, 0, 0);
                    }
                }
            }

            string startParamUri = string.Format("start={0}", startDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss"));
            string endParamUri = string.Format("stop={0}", endDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss"));

            /// <summary>
            /// {0}   = https://api.prism.com/
            /// {1}   = v2
            /// {2}   = data/by-time/?
            /// {3}   = business_hours_only=false
            /// {4}   = period=hour
            /// {5}   = metric=count&metric=avg_dwell&metric=avg_occupancy
            /// {6}   = zone_id=
            /// {7}   = site_id=
            /// {8}   = start=
            /// {9}   = stop=
            /// </summary>

            return string.Format("{0}{1}{2}{3}&{4}&{5}&{6}&{7}&{8}&{9}",
                this.Uri,
                this.Version,
                this.DataByTimeFilter,
                this.BusinessHoursOnly,
                this.PeriodByHour,
                this.MetricByAll,
                zoneIdParamUri,
                siteIdParamUri,
                startParamUri,
                endParamUri
                );
        }

        // TODO Try to create a base HttpClient Request function
        public virtual void TestFunction()
        {

        }

        private DateTime? GetLatestDate()
        {
            string query = @"Select MAX([Date]) From DoorCounterReport WHERE VendorName = 'Prism'";

            using (SqlConnection con = new SqlConnection(sqlconnectionstring))
            {
                try
                {
                    con.Open();
                    var latestDate = con.ExecuteScalar(query);
                    return latestDate == null ? (DateTime?)null : Convert.ToDateTime(latestDate);
                }
                catch (Exception ex)
                {
                    Logger.Error("BaseService | GetLatestDate | error message: {0}", ex.Message);
                    return null;
                }
            }
        }
    }
}
