﻿namespace DoorCounter.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Models;

    public class AccountService : BaseService
    {
        private PrismModel _prismModel;

        public AccountService()
        {
            _prismModel = GetPrism();
        }

        private PrismModel GetPrism()
        {
            PrismModel model = new PrismModel();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(this.Uri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", this.Token);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.HeaderValue));
                    HttpResponseMessage response = client.GetAsync(this.Version).Result;
                    model = response.Content.ReadAsAsync<PrismModel>().Result;
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.Error("AccountService | GetPrism | error message: {0}", ex.Message);
            }

            return model;
        }

        public IEnumerable<AccountModel> GetAccounts()
        {
            IEnumerable<AccountModel> list = Enumerable.Empty<AccountModel>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(this._prismModel.Accounts_Url);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", this.Token);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.HeaderValue));
                    HttpResponseMessage response = client.GetAsync(string.Empty).Result;
                    list = response.Content.ReadAsAsync<IEnumerable<AccountModel>>().Result;
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.Error("AccountService | GetAccounts | error message: {0}", ex.Message);
            }

            if (!string.IsNullOrEmpty(this.Accounts))
            {
                var accounts = this.Accounts.Split(',');
                list = from a in list
                       where accounts.Contains(a.Id.ToString())
                       select a;
            }

            return list;
        }

        public PrismModel Prism {
            get { return _prismModel; }
        }
    }
}
