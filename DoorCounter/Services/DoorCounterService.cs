﻿namespace DoorCounter.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Models;
    using Models.Custom;
    using Repository.Implement;
    using Repository.Interface;
    using Utils;

    public class DoorCounterService : BaseService
    {
        // TODO dependency injection
        //private IDoorCounterRepository doorRepository;
        //public ZoneDataService(IDoorCounterRepository _doorRepository)
        //{
        //    doorRepository = _doorRepository;
        //}

        private readonly IDoorCounter _doorRepository = new DoorCounter();

        public List<DoorCounterModel> GetDoorCounterList(List<ZoneSiteModel> list)
        {
            List<DoorCounterModel> doorCounterList = new List<DoorCounterModel>();

            try
            {
                foreach (var item in list)
                {
                    /// <summary>
                    /// get data by time filter
                    /// </summary>
                    using (var client = new HttpClient())
                    {
                        string dataByTimeUri = GetTimeFilterUri(item.ZoneId.ToString(), item.SiteId.ToString());
                        client.BaseAddress = new Uri(dataByTimeUri);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", this.Token);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.HeaderValue));
                        HttpResponseMessage response = client.GetAsync(string.Empty).Result;
                        var result = response.Content.ReadAsAsync<IEnumerable<DataByTimeModel>>().Result;

                        int index = 1;
                        foreach (var data in result)
                        {
                            doorCounterList.Add(new DoorCounterModel
                            {
                                SiteId = item.SiteId,
                                CustomerCID = item.ExternalId,
                                CustomerName = item.SiteName,
                                Date = Convert.ToDateTime(data.Start),
                                Hour = index,
                                Count = Convert.ToInt32(data.Count),
                                DoorDateKey =
                                    $"{item.ExternalId}{DateUtils.DateToYyyyMmDd(Convert.ToDateTime(data.Start))}{index.ToString().PadLeft(2, '0')}",
                                VendorName = "Prism"
                            });
                            index = index == 24 ? 1 : index + 1;
                        }
                    }
                }

                /// <summary>
                /// insert to database
                /// </summary>
                _doorRepository.InsertBulk(doorCounterList);
            }
            catch (Exception ex)
            {
                Logger.Error("DoorCounterService | GetDoorCounterList | error message: {0}", ex.Message);
            }

            return doorCounterList;
        }
    }
}
