﻿namespace DoorCounter.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Models;

    public class ZoneService : BaseService
    {
        public IEnumerable<ZoneModel> GetAllZones(string uri)
        {
            IEnumerable<ZoneModel> zones = Enumerable.Empty<ZoneModel>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(uri);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", this.Token);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(this.HeaderValue));
                    HttpResponseMessage response = client.GetAsync(string.Empty).Result;
                    zones = response.Content.ReadAsAsync<IEnumerable<ZoneModel>>().Result;
                }
            }
            catch (HttpRequestException ex)
            {
                Logger.Error("ZoneService | GetAllZones | error message: {0}", ex.Message);
            }

            return zones;
        }
    }
}
